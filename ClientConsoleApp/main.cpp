#include <iostream>
#include <string>
#include <filesystem>
#include "File.hpp"
#include "Database.hpp"

int main()
{
  std::cout << "Type a file or path, which you want to share" << std::endl;
  std::string fromUser;
  std::getline(std::cin, fromUser);
  File file(fromUser);
  if (!file.is_exists())
  {
    std::cout << "Can't open this file" << std::endl;
    return 0;
  }
  if (file.addFile(false) == -1)
  {
    std::cout << "Some errors" << std::endl;
    return 0;
  }
  std::cout << "Enter your nickname in Telegram, if it exists" << std::endl;
  std::getline(std::cin, fromUser);
  std::cout << "Description for the file?" << std::endl;
  std::string description;
  std::getline(std::cin, description);
  Database database;
  database.addRecord(file.getHash(), file.getFileName(), fromUser, description);
  std::cout << "Success\nHash: " + file.getHash() << std::endl;
  return 0;
}