#include <iostream>
#include <string>
#include <filesystem>
#include "md5wrapper.h"
#include "File.hpp"
#include "Database.hpp"
#include "ClientGUIApp.h"
#include "ui_ClientGUIApp.h"
#include "TableFile.h"
#include "QFileDialog"
#include "QMessageBox"

void ClientGUIApp::initializationText()
{
  ui.Username->setPlaceholderText("UserName...");
  ui.Description->setPlaceholderText("Description...");
  ui.user_text->setPlaceholderText("Enter some string, if you want");
}

ClientGUIApp::ClientGUIApp(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);
    initializationText();
}

void ClientGUIApp::on_choosingFile_clicked()
{
  ui.choosedFile->setText(QFileDialog::getOpenFileName(0, "Open Dialog", "", "*.*"));
}

void ClientGUIApp::on_startUpload_clicked()
{
  File file(ui.choosedFile->text().toStdString());
  if (!file.is_exists())
  {
    QMessageBox::warning(0,
                         "Error",
                         "Can't open this file",
                         "Ok",
                          QString(), QString(), 0, 1);
    return;
  }
  if (file.addFile(ui.deletingSource->isChecked()) == -1)
  {
    QMessageBox::warning(0,
                         "Error",
                         "Some others error",
                         "Ok",
                         QString(), QString(), 0, 1);
    return;
  }

  Database database;
  database.addRecord(file.getHash(), file.getFileName(), ui.Username->text().toStdString(), ui.Description->text().toStdString());
  ui.Output_Hash_file->setText(QString::fromStdString(file.getHash()));
  QMessageBox::information(0, "Information", "Success!");
}

void ClientGUIApp::on_gettingHash_clicked()
{
  md5wrapper md5;
  ui.Output_Hash_text->setText(QString::fromStdString(md5.getHashFromString(ui.user_text->toPlainText().toStdString())));
}

void ClientGUIApp::on_list_of_file_clicked()
{
  TableFile *table = new TableFile(this);
  table->show();
}