#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_ClientGUIApp.h"

class ClientGUIApp : public QMainWindow
{
    Q_OBJECT

public:
    ClientGUIApp(QWidget *parent = Q_NULLPTR);

private slots:
    void on_choosingFile_clicked();
    void on_startUpload_clicked();
    void on_gettingHash_clicked();
    void on_list_of_file_clicked();
private:
    Ui::ClientGUIAppClass ui;
    void initializationText();
};
