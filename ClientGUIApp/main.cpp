#include "ClientGUIApp.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ClientGUIApp w;
    w.show();
    return a.exec();
}
