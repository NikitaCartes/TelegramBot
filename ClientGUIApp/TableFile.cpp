#include <iostream>
#include <string>
#include <filesystem>
#include "md5wrapper.h"
#include "File.hpp"
#include "Database.hpp"
#include "TableFile.h"
#include "ui_TableFile.h"
#include "QFileDialog"
#include "QMessageBox"

void TableFile::initializationTable()
{
  TableUi.TableFile->clear();
  TableUi.TableFile->setColumnCount(5);
  Database database;
  int rows = database.numberOfRecord();
  //TableUi.TableFile->setRowCount(rows);
  TableUi.TableFile->setShowGrid(true);
  TableUi.TableFile->setSelectionMode(QAbstractItemView::SingleSelection);
  TableUi.TableFile->setSelectionBehavior(QAbstractItemView::SelectRows);
  TableUi.TableFile->setHorizontalHeaderLabels(QStringList() << trUtf8("id") << trUtf8("Hash") << trUtf8("Filename") << trUtf8("Owner") << trUtf8("Description"));
  TableUi.TableFile->horizontalHeader()->setStretchLastSection(true);
  TableUi.TableFile->hideColumn(0);
  for (int i = 0; i < rows; i++)
  {
    std::string row = database.Row(i+1);
    TableUi.TableFile->insertRow(i);
    TableUi.TableFile->setItem(i, 0, new QTableWidgetItem(i));
    TableUi.TableFile->setItem(i, 1, new QTableWidgetItem(QString::fromStdString(row.substr(2, 32))));
    size_t endOfName = row.find(">", 36);
    size_t endOfNick = row.find(">", endOfName + 1);
    TableUi.TableFile->setItem(i, 2, new QTableWidgetItem(QString::fromStdString(row.substr(36, endOfName - 36))));
    TableUi.TableFile->setItem(i, 3, new QTableWidgetItem(QString::fromStdString(row.substr(endOfName + 2, endOfNick - endOfName - 2))));
    TableUi.TableFile->setItem(i, 4, new QTableWidgetItem(QString::fromStdString(row.substr(endOfNick + 2, row.length() - endOfNick - 4))));
  }
  TableUi.TableFile->setEditTriggers(QTableWidget::NoEditTriggers);
  TableUi.TableFile->resizeColumnsToContents();
}

TableFile::TableFile(QWidget *parent)
  : QMainWindow(parent)
{
  TableUi.setupUi(this);
  initializationTable();
}