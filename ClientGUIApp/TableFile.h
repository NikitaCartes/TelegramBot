#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_TableFile.h"

class TableFile : public QMainWindow
{
  Q_OBJECT

public:
  TableFile(QWidget *parent = Q_NULLPTR);

private slots:
  
private:
  Ui::TableFileClass TableUi;
  void initializationTable();
};