/********************************************************************************
** Form generated from reading UI file 'ClientGUIApp.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLIENTGUIAPP_H
#define UI_CLIENTGUIAPP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ClientGUIAppClass
{
public:
    QWidget *centralWidget;
    QLineEdit *Username;
    QLineEdit *Description;
    QPushButton *startUpload;
    QWidget *layoutWidget;
    QHBoxLayout *Layout_ChooseFile;
    QLineEdit *choosedFile;
    QPushButton *choosingFile;
    QFrame *line;
    QLabel *label_hash_text;
    QLabel *label_hash_file;
    QWidget *layoutWidget_2;
    QVBoxLayout *Layout_Hash_text;
    QLabel *label_output_Hash_text;
    QLineEdit *Output_Hash_text;
    QPushButton *gettingHash;
    QTextEdit *user_text;
    QWidget *layoutWidget1;
    QVBoxLayout *Layout_Hash_File;
    QLabel *label_output_Hash_file;
    QLineEdit *Output_Hash_file;
    QCheckBox *deletingSource;
    QFrame *line_2;
    QPushButton *list_of_file;

    void setupUi(QMainWindow *ClientGUIAppClass)
    {
        if (ClientGUIAppClass->objectName().isEmpty())
            ClientGUIAppClass->setObjectName(QStringLiteral("ClientGUIAppClass"));
        ClientGUIAppClass->resize(880, 530);
        QFont font;
        font.setUnderline(false);
        ClientGUIAppClass->setFont(font);
        centralWidget = new QWidget(ClientGUIAppClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        Username = new QLineEdit(centralWidget);
        Username->setObjectName(QStringLiteral("Username"));
        Username->setGeometry(QRect(474, 90, 113, 20));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Username->sizePolicy().hasHeightForWidth());
        Username->setSizePolicy(sizePolicy);
        Username->setLayoutDirection(Qt::LeftToRight);
        Username->setAlignment(Qt::AlignCenter);
        Description = new QLineEdit(centralWidget);
        Description->setObjectName(QStringLiteral("Description"));
        Description->setGeometry(QRect(644, 90, 113, 20));
        Description->setAlignment(Qt::AlignCenter);
        startUpload = new QPushButton(centralWidget);
        startUpload->setObjectName(QStringLiteral("startUpload"));
        startUpload->setGeometry(QRect(520, 250, 211, 61));
        QFont font1;
        font1.setPointSize(16);
        font1.setBold(true);
        font1.setUnderline(false);
        font1.setWeight(75);
        startUpload->setFont(font1);
        startUpload->setIconSize(QSize(16, 16));
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(434, 210, 391, 25));
        Layout_ChooseFile = new QHBoxLayout(layoutWidget);
        Layout_ChooseFile->setSpacing(6);
        Layout_ChooseFile->setContentsMargins(11, 11, 11, 11);
        Layout_ChooseFile->setObjectName(QStringLiteral("Layout_ChooseFile"));
        Layout_ChooseFile->setContentsMargins(0, 0, 0, 0);
        choosedFile = new QLineEdit(layoutWidget);
        choosedFile->setObjectName(QStringLiteral("choosedFile"));
        choosedFile->setAlignment(Qt::AlignCenter);

        Layout_ChooseFile->addWidget(choosedFile);

        choosingFile = new QPushButton(layoutWidget);
        choosingFile->setObjectName(QStringLiteral("choosingFile"));
        choosingFile->setEnabled(true);

        Layout_ChooseFile->addWidget(choosingFile);

        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(390, 0, 16, 511));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        label_hash_text = new QLabel(centralWidget);
        label_hash_text->setObjectName(QStringLiteral("label_hash_text"));
        label_hash_text->setGeometry(QRect(60, 40, 251, 31));
        QFont font2;
        font2.setPointSize(16);
        font2.setBold(true);
        font2.setWeight(75);
        label_hash_text->setFont(font2);
        label_hash_text->setAlignment(Qt::AlignCenter);
        label_hash_file = new QLabel(centralWidget);
        label_hash_file->setObjectName(QStringLiteral("label_hash_file"));
        label_hash_file->setGeometry(QRect(480, 30, 271, 31));
        label_hash_file->setFont(font2);
        label_hash_file->setAlignment(Qt::AlignCenter);
        layoutWidget_2 = new QWidget(centralWidget);
        layoutWidget_2->setObjectName(QStringLiteral("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(60, 410, 261, 41));
        Layout_Hash_text = new QVBoxLayout(layoutWidget_2);
        Layout_Hash_text->setSpacing(6);
        Layout_Hash_text->setContentsMargins(11, 11, 11, 11);
        Layout_Hash_text->setObjectName(QStringLiteral("Layout_Hash_text"));
        Layout_Hash_text->setContentsMargins(0, 0, 0, 0);
        label_output_Hash_text = new QLabel(layoutWidget_2);
        label_output_Hash_text->setObjectName(QStringLiteral("label_output_Hash_text"));
        label_output_Hash_text->setAlignment(Qt::AlignCenter);

        Layout_Hash_text->addWidget(label_output_Hash_text);

        Output_Hash_text = new QLineEdit(layoutWidget_2);
        Output_Hash_text->setObjectName(QStringLiteral("Output_Hash_text"));
        sizePolicy.setHeightForWidth(Output_Hash_text->sizePolicy().hasHeightForWidth());
        Output_Hash_text->setSizePolicy(sizePolicy);
        Output_Hash_text->setLayoutDirection(Qt::LeftToRight);
        Output_Hash_text->setAlignment(Qt::AlignCenter);
        Output_Hash_text->setReadOnly(true);

        Layout_Hash_text->addWidget(Output_Hash_text);

        gettingHash = new QPushButton(centralWidget);
        gettingHash->setObjectName(QStringLiteral("gettingHash"));
        gettingHash->setGeometry(QRect(70, 340, 231, 61));
        gettingHash->setFont(font1);
        gettingHash->setIconSize(QSize(16, 16));
        user_text = new QTextEdit(centralWidget);
        user_text->setObjectName(QStringLiteral("user_text"));
        user_text->setGeometry(QRect(60, 80, 251, 251));
        layoutWidget1 = new QWidget(centralWidget);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(500, 340, 251, 41));
        Layout_Hash_File = new QVBoxLayout(layoutWidget1);
        Layout_Hash_File->setSpacing(6);
        Layout_Hash_File->setContentsMargins(11, 11, 11, 11);
        Layout_Hash_File->setObjectName(QStringLiteral("Layout_Hash_File"));
        Layout_Hash_File->setContentsMargins(0, 0, 0, 0);
        label_output_Hash_file = new QLabel(layoutWidget1);
        label_output_Hash_file->setObjectName(QStringLiteral("label_output_Hash_file"));
        label_output_Hash_file->setAlignment(Qt::AlignCenter);

        Layout_Hash_File->addWidget(label_output_Hash_file);

        Output_Hash_file = new QLineEdit(layoutWidget1);
        Output_Hash_file->setObjectName(QStringLiteral("Output_Hash_file"));
        sizePolicy.setHeightForWidth(Output_Hash_file->sizePolicy().hasHeightForWidth());
        Output_Hash_file->setSizePolicy(sizePolicy);
        Output_Hash_file->setLayoutDirection(Qt::LeftToRight);
        Output_Hash_file->setAlignment(Qt::AlignCenter);
        Output_Hash_file->setReadOnly(true);

        Layout_Hash_File->addWidget(Output_Hash_file);

        deletingSource = new QCheckBox(centralWidget);
        deletingSource->setObjectName(QStringLiteral("deletingSource"));
        deletingSource->setGeometry(QRect(480, 160, 141, 20));
        deletingSource->setLayoutDirection(Qt::LeftToRight);
        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(400, 390, 471, 20));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);
        list_of_file = new QPushButton(centralWidget);
        list_of_file->setObjectName(QStringLiteral("list_of_file"));
        list_of_file->setGeometry(QRect(520, 410, 211, 61));
        list_of_file->setFont(font1);
        list_of_file->setIconSize(QSize(16, 16));
        ClientGUIAppClass->setCentralWidget(centralWidget);

        retranslateUi(ClientGUIAppClass);

        QMetaObject::connectSlotsByName(ClientGUIAppClass);
    } // setupUi

    void retranslateUi(QMainWindow *ClientGUIAppClass)
    {
        ClientGUIAppClass->setWindowTitle(QApplication::translate("ClientGUIAppClass", "ClientGUIApp", nullptr));
        Username->setText(QString());
        Description->setText(QString());
        startUpload->setText(QApplication::translate("ClientGUIAppClass", "Add to database", nullptr));
        choosingFile->setText(QApplication::translate("ClientGUIAppClass", "Choose file", nullptr));
        label_hash_text->setText(QApplication::translate("ClientGUIAppClass", "Get hash from your text", nullptr));
        label_hash_file->setText(QApplication::translate("ClientGUIAppClass", "Get hash from your file", nullptr));
        label_output_Hash_text->setText(QApplication::translate("ClientGUIAppClass", "Hash of your text", nullptr));
        Output_Hash_text->setText(QString());
        gettingHash->setText(QApplication::translate("ClientGUIAppClass", "Get hash", nullptr));
        label_output_Hash_file->setText(QApplication::translate("ClientGUIAppClass", "Hash of your file", nullptr));
        Output_Hash_file->setText(QString());
        deletingSource->setText(QApplication::translate("ClientGUIAppClass", "Delete source file", nullptr));
        list_of_file->setText(QApplication::translate("ClientGUIAppClass", "List of file", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ClientGUIAppClass: public Ui_ClientGUIAppClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLIENTGUIAPP_H
