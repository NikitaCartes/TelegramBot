/********************************************************************************
** Form generated from reading UI file 'TableFile.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TABLEFILE_H
#define UI_TABLEFILE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TableFileClass
{
public:
    QWidget *centralwidget;
    QTableWidget *TableFile;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *TableFileClass)
    {
        if (TableFileClass->objectName().isEmpty())
            TableFileClass->setObjectName(QStringLiteral("TableFileClass"));
        TableFileClass->resize(800, 600);
        centralwidget = new QWidget(TableFileClass);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        TableFile = new QTableWidget(centralwidget);
        TableFile->setObjectName(QStringLiteral("TableFile"));
        TableFile->setGeometry(QRect(10, 10, 781, 541));
        TableFileClass->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(TableFileClass);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        TableFileClass->setStatusBar(statusbar);

        retranslateUi(TableFileClass);

        QMetaObject::connectSlotsByName(TableFileClass);
    } // setupUi

    void retranslateUi(QMainWindow *TableFileClass)
    {
        TableFileClass->setWindowTitle(QApplication::translate("TableFileClass", "MainWindow", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TableFileClass: public Ui_TableFileClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TABLEFILE_H
