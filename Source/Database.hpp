#ifndef DATABASE_HPP
#define DATABASE_HPP

#include <string>
#include <fstream>
#include "File.hpp"

class Database
{
  public:
    Database();
    void addRecord(const std::string, const std::string, const std::string, const std::string);
    int numberOfRecord();
    std::string Row(const int);
    ~Database();
  private:
    std::string path_;
    std::fstream fout;
};

#endif