#ifndef FILE_HPP
#define FILE_HPP

#include <string>
#include <filesystem>


const std::string DIR = "/";

class File
{
  public:
    File(const std::string);
    int addFile(const bool);
    std::string getFileName() const;
    std::string getHash() const;
    bool is_exists() const;
  private:
    bool exist;
    std::string hash_;
    std::experimental::filesystem::path path_;
};

#endif