#include "File.hpp"
#include "md5wrapper.h"

File::File(const std::string filename) :
  path_(filename)
{
  if (std::experimental::filesystem::exists(path_))
  {
    exist = true;
  }
  else
  {
    exist = false;
  }
  if (exist)
  {
    md5wrapper md5;
    hash_ = md5.getHashFromFile(path_.string());
    //md5.~md5wrapper();
  }
}

int File::addFile(const bool deleteSourceFile)
{
  if (std::experimental::filesystem::exists(DIR + "data/copy/" + hash_))
  {
    return 1;
  }
  try
  {
    std::experimental::filesystem::create_directories(DIR + "data/copy");
    if (deleteSourceFile)
    {
      std::experimental::filesystem::rename(path_, DIR + "data/copy/" + hash_);
    }
    else
    {
      std::experimental::filesystem::copy_file(path_, DIR + "data/copy/" + hash_);
    }
  }
  catch (std::experimental::filesystem::filesystem_error&)
  {
    return -1;
  }
  return 0;
}

std::string File::getFileName() const
{
  return path_.filename().string();
}

std::string File::getHash() const
{
  return hash_;
}

bool File::is_exists() const
{
  return exist;
}