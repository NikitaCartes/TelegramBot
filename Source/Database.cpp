#include "Database.hpp"

Database::Database() :
  path_(DIR + "data/files.nctb")
{
  fout.open(path_, std::ios::app);
}

Database::~Database()
{
  fout.close();
}

void Database::addRecord(const std::string hash, const std::string filename, const std::string nickname, const std::string description)
{
  fout << "<<" << hash << "><" << filename << "><" << nickname << "><" << description << ">>" << std::endl;
}

int Database::numberOfRecord()
{
  std::fstream ftemp;
  ftemp.open(path_);
  int i = 0;
  std::string line;
  while (getline(ftemp, line))
  {
    i++;
  }
  ftemp.close();
  return i;
}

std::string Database::Row(const int row)
{
  std::fstream ftemp;
  ftemp.open(path_);
  int i = 0;
  std::string line;
  while (i != row)
  {
    getline(ftemp, line);
    i++;
  }
  ftemp.close();
  return line;
}
