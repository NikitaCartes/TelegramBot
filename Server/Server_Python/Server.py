import telebot
from pathlib import Path
import requests
import io
from collections import deque
from cpluslusmodule import addFile
import shutil
import os

API_TOKEN = '' # Todo Удалить токен
DIR = '/'

bot = telebot.TeleBot(API_TOKEN)

# Handle '/start' and '/help'
@bot.message_handler(commands=['help', 'start'])
def send_welcome(message):
    bot.reply_to(message, "Send me a unique code!\nAlso:\n/list")

@bot.message_handler(commands=['list'])
def send_welcome(message):
    out = ""
    f = open(DIR + 'data/files.nctb')
    while True:
        line = f.readline()
        if not line:
            break
        endOfName = line.find(">", 36)
        endOfNick = line.find(">", endOfName+1)
        if ((str(message.from_user.id) == line[endOfName+2:endOfNick]) or (message.from_user.username == line[endOfName+2:endOfNick])):
            out = out + "/" + line[2:34] + " as a " + line[36:endOfName] + "\n"
    f.close()
    if out == "" :
        out = "You didn't haму any files"
    bot.send_message(message.chat.id, out)
    print("Send list of file to " + message.from_user.username)

@bot.message_handler(content_types=['document'])
def download_files(message):
    print("New file from " + message.from_user.username)
    file_info = bot.get_file(message.document.file_id)
    file = requests.get('https://api.telegram.org/file/bot{0}/{1}'.format(API_TOKEN, file_info.file_path))
    with open(DIR + 'data/newfiles/' + message.document.file_name, "wb") as code:
        code.write(file.content)
    #path = DIR + 'data/newfiles/' + message.document.file_name
    #nickname = message.from_user.username
    hash = addFile(message.from_user.id)
    bot.reply_to(message, hash)

# Handle all other messages with content_type 'text' (content_types defaults to ['text'])
@bot.message_handler(func=lambda message: True)
def echo_message(message):
    if message.text[0] == "/" :
        message.text = message.text[1:]
    my_file = Path(DIR + 'data/copy/' + message.text)
    if my_file.is_file():

        f = open(DIR + 'data/files.nctb')
        stat = f.readline()
        while (message.text != stat[2:34]):
            stat=f.readline()
        endOfName = stat.find(">", 36)

        os.rename(DIR + 'data/copy/' + message.text, DIR + 'data/copy/' + stat[36:endOfName])

        doc = open(DIR + 'data/copy/' + stat[36:endOfName], 'rb')
        bot.send_document(message.chat.id, doc)
        doc.close()

        os.rename(DIR + 'data/copy/' + stat[36:endOfName], DIR + 'data/copy/' + message.text)
        print("Send " + message.text + " to " + message.from_user.username)
    else:
        bot.reply_to(message, "Sorry, but i haven't this file")
        print("Not send " + message.text + " to " + message.from_user.username)
    #bot.reply_to(message, message.text)
try:
    bot.polling()
except Exception:
    print("No internet or another error")