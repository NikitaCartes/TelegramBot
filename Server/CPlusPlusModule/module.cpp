#include <Python.h>
#include <iostream>
#include <string>
#include <filesystem>
#include "File.hpp"
#include "Database.hpp"

PyObject* add(PyObject *self, PyObject *id) {
  long int user_id = PyLong_AsLong(id);
  for (auto& p : std::experimental::filesystem::directory_iterator(DIR + "data/newfiles/"))
  {
    File file(p.path().string());
    if (file.addFile(true) == 0)
    {
      Database database;
      database.addRecord(file.getHash(), file.getFileName(), std::to_string(user_id), "");
    }
    return  PyBytes_FromString(file.getHash().c_str());
  }
  return  PyLong_FromLong(0);
}

static PyMethodDef cpluslusmodule_methods[] = {
  // The first property is the name exposed to python, the second is the C++ function name        
  { "addFile", (PyCFunction)add, METH_O, nullptr },

  // Terminate the array with an object containing nulls.
{ nullptr, nullptr, 0, nullptr }
};

static PyModuleDef cpluslusmodule_module = {
  PyModuleDef_HEAD_INIT,
  "cpluslusmodule",                        // Module name
  "Provides some functions, but faster",  // Module description
  0,
  cpluslusmodule_methods                   // Structure that defines the methods
};

PyMODINIT_FUNC PyInit_cpluslusmodule() {
  return PyModule_Create(&cpluslusmodule_module);
}